package net.beanscode.plugin.links;

import net.beanscode.model.notebook.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.Meta;

public class LinkEntry extends NotebookEntry {
	
	private final String SOURCE_NOTEBOOK_QUERY = "sourceNotebookQuery";
	private final String SOURCE_ENTRY_QUERY = "sourceEntryQuery";
	private final String SOURCE_NOTEBOOK_ID = "sourceNotebookId";
	private final String SOURCE_ENTRY_ID = "sourceEntryId";

	public LinkEntry() {
		
	}
	
	public LinkEntry setSourceEntryId(UUID sourceEntryId) {
		getMeta().add(SOURCE_ENTRY_ID, sourceEntryId);
		return this;
	}
	
	public UUID getSourceEntryId() {
		Meta meta = getMeta().get(SOURCE_ENTRY_ID);
		return meta != null ? new UUID(meta.getAsString()) : null;
	}
	
	public String getSourceEntryQuery() {
		return getMeta().getAsString(SOURCE_ENTRY_QUERY);
	}
	
	public String getSourceNotebookQuery() {
		return getMeta().getAsString(SOURCE_NOTEBOOK_QUERY);
	}
	
	public LinkEntry setSourceNotebookId(UUID sourceNotebookId) {
		getMeta().add(SOURCE_NOTEBOOK_ID, sourceNotebookId);
		return this;
	}
	
	public UUID getSourceNotebookId() {
		Meta meta = getMeta().get(SOURCE_NOTEBOOK_ID);
		return meta != null ? new UUID(meta.getAsString()) : null;
	}
	
	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plots.LinksEntryEditorPanel";
	}
	
	@Override
	public String getShortDescription() {
		return "Link (create a link to an entry(ies) from another notebook)";
	}
}
